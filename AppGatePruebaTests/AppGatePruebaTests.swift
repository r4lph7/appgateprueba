//
//  AppGatePruebaTests.swift
//  AppGatePruebaTests
//
//  Created by RalphM on 7/08/21.
//

import XCTest
import AppGateLibrary
@testable import AppGatePrueba

class AppGatePruebaTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testEmailAndPassword() throws {
        let registerViewModel = RegisterViewModel.shared
        
        XCTAssertTrue(registerViewModel.validateEmail("ralph.manga07@gmail.com")) //Valid Email
        XCTAssertTrue(registerViewModel.validateEmail("m@mail.com")) //Valid Email
        XCTAssertFalse(registerViewModel.validateEmail("m@mailcom")) //Invalid Email
        XCTAssertFalse(registerViewModel.validateEmail("mail.com")) //Invalid Email
        
        XCTAssertTrue(registerViewModel.validatePassword("S3cur3P@ssword")) //Valid Strong Password
        XCTAssertTrue(registerViewModel.validatePassword("BigSecur3d@@")) //Valid Strong Password
        XCTAssertFalse(registerViewModel.validatePassword("insecurePass1")) //Invalid Strong Password
        XCTAssertFalse(registerViewModel.validatePassword("Pass@1")) //Invalid Strong Password
    }
    
    func testAccountOnAppGateLibrary() throws {
        //Run once and then change userA username.
        
        let presenter = AppGateLibraryPresenter.shared
        let userA = User(username: "ralph.manga07@gmail.com", password: "S3cur3P@ssword")
        let userB = User(username: "ralph@gmail.com", password: "S3cur3P@ssword")
        presenter.registerUser(userA, completion: { succ in
            XCTAssertTrue(succ) // User Registred
        })
        presenter.registerUser(userA, completion: { succ in
            XCTAssertFalse(succ) // User Already Registred
        })
        presenter.validateAccount(userA, completion: { succ in
            XCTAssertTrue(succ) // User Validate
        })
        presenter.validateAccount(userB, completion: { succ in
            XCTAssertFalse(succ) // User Validate (Non Exist)
        })
    }

}
