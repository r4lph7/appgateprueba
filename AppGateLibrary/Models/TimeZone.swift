//
//  TimeZone.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation

public struct TimeZone : Codable {
    let sunrise: String
    let lng: Double
    let countryCode: String
    let gmtOffset: Int
    let rawOffset: Int
    let sunset: String
    let timezoneId: String
    let dstOffset: Int
    let countryName: String
    public let time: String
    let lat: Double
}
