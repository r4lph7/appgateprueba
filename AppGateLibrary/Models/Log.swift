//
//  Log.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation


public struct Log : Codable {
    public enum Result : String, Codable {
        case success = "Success"
        case denied = "Denied"
    }
    public var username : String
    public var result : Result
    public var timezone : TimeZone
}
