//
//  User.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation


public struct User : Codable {
    public var username : String
    public var password : String
    
    public init(username: String, password: String){
        self.username = username.lowercased()
        self.password = password
    }
}
