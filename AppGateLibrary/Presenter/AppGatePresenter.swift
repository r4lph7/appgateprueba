//
//  AppGateLibraryPresenter.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation


public class AppGateLibraryPresenter {
    
    public static var shared = AppGateLibraryPresenter()
    
    private var isAuthorized : Bool = false
    
    let locationManager = LocationManager()
    
    public func readLogs() -> [Log]? {
        if isAuthorized {
            return AccountManager.shared.getLogs()
        }else{
            return nil
        }
    }
    
    public func registerUser(_ user: User, completion: @escaping(Bool) -> Void){
        AccountManager.shared.registerUser(user: user) { succ in
            completion(succ)
        }
    }
    
    public func validateAccount(_ user: User, completion: @escaping(Bool)->Void){
        isAuthorized = AccountManager.shared.validateUser(user)
        saveAttemp(user: user, result: isAuthorized ? .success : .denied){
            completion(self.isAuthorized)
        }
    }
    
    public func saveAttemp(user: User, result: Log.Result, completion: @escaping()->Void?){
        let coordinates = locationManager.getLocation()
        TimeZoneService.getTimeZone(latitude: coordinates.latitude, longitude: coordinates.longitude) { timezone in
            if let timezone = timezone {
                let log = Log(username: user.username, result: result, timezone: timezone)
                AccountManager.shared.registerLog(log: log){
                    completion()
                }
            }
        }
    }
}

