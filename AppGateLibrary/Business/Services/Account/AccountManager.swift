//
//  AccountManager.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import AppGateStorage

public class AccountManager : NSObject {
    
    public static let shared = AccountManager()
    
    func getLogs() -> [Log] {
        return getData(key: .logs, modelTye: Log.self, secure: false)
    }
    
    func registerUser(user: User, completionHandler: @escaping (Bool) -> Void){
        if userExists(username: user.username), checkUserData(user) {
           completionHandler(false)
        }else{
            registerData(data: user, key: .accounts, modelTye: User.self) { success in
                completionHandler(success)
            }
        }
    }
    
    func registerLog(log: Log, completionHandler: @escaping () -> Void){
        registerData(data: log, key: .logs, modelTye: Log.self, secure: false) { success in
            completionHandler()
        }
    }
    
    func checkUserData(_ user: User) -> Bool{
        let passwordPred = NSPredicate(format: "SELF MATCHES %@ ", "^(?=.*[a-z])(?=.*[$@$#!%*?&])(?=.*[0-9])(?=.*[A-Z]).{8,}$")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: user.username) && passwordPred.evaluate(with: user.password)
    }
    
    func validateUser(_ user: User) -> Bool {
        let users = getData(key: .accounts, modelTye: User.self)
        return users.contains { $0.username == user.username && $0.password == user.password }
    }
}

private extension AccountManager {
    func userExists(username: String) -> Bool {
        let users = getData(key: .accounts, modelTye: User.self)
        return users.contains { $0.username == username }
    }
    
    func registerData<T: Codable>(data: T, key : Key ,modelTye: T.Type, secure: Bool = true, completionHandler: @escaping (Bool)-> Void){
        var list = getData(key: key, modelTye: modelTye, secure: secure)
        list.append(data)
        if let encodedData = try? PropertyListEncoder().encode(list) {
            do {
                try AppGateStoragePresenter.set(value: encodedData, account:key.rawValue, secure: secure)
                completionHandler(true)
            }catch let error {
                print(error)
                completionHandler(false)
            }
        }
    }
    
    func getData<T:Codable>(key : Key ,modelTye: T.Type, secure: Bool = true) -> [T] {
        if let encodedData = try? AppGateStoragePresenter.get(account: key.rawValue, secure: secure),
           let unencodedData = try? PropertyListDecoder().decode([T].self, from: encodedData) {
            return unencodedData
        }else{
            return []
        }
    }
}
