//
//  TimeZoneService.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation

typealias timeZoneCompletionHandler = (TimeZone?) -> Void

struct TimeZoneService {
    static func getTimeZone(latitude: Double, longitude: Double, completionHandler: @escaping timeZoneCompletionHandler) {
        let url = "\(URLRepository.TimeZoneURL.baseURL)&lat=\(String(latitude))&lng=\(String(longitude))"
        APIClient().requestHttpwithUrl(serviceURL: url, method: .GET, withData: [:], modelType: TimeZone.self) { success, data in
            if success {
                completionHandler(data)
            }else{
                completionHandler(nil)
            }
        }
    }
}
