//
//  LocationManager.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    var locationManager = CLLocationManager()
    var defaultLocation = CLLocationCoordinate2D(latitude: 4.71, longitude: -74.07)
    
    override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func getLocation() -> CLLocationCoordinate2D {
        if let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate {
            return locValue
        }
        return defaultLocation
    }
}
