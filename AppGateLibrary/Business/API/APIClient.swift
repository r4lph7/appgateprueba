//
//  APIClient.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import Combine
 
class APIClient: NSObject {
    public func requestHttpwithUrl<T : Codable>(serviceURL:String, method: URLRepository.Method, withData parameters: [String:Any], modelType:T.Type, completionHandler: @ escaping (Bool, T?) -> Void) {
        
        let request_url = URL(string: serviceURL)
        
        let request:NSMutableURLRequest = NSMutableURLRequest()
        request.url = request_url
        request.httpMethod = method.rawValue
        request.timeoutInterval=30
        
        if method != .GET {
            let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = postData
        }
        
        let session = URLSession.init(configuration: .default)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let safeData = data {
                let decoder = JSONDecoder()
                do {
                    let decodedData = try decoder.decode(modelType, from: safeData)
                    completionHandler(true, decodedData)
                } catch {
                    completionHandler(false, nil)
                }
            }
        })
        
        task.resume()
    }
}
