//
//  URLRepository.swift
//  AppGateLibrary
//
//  Created by RalphM on 7/08/21.
//

import Foundation

struct URLRepository {
    enum Method : String {
        case GET
        case POST
    }
    
    struct TimeZoneURL {
        static let baseURL: String = "http://api.geonames.org/timezoneJSON?formatted=true&username=qa_mobile_easy&style=full"
        static let method: Method = .GET
    }
}
