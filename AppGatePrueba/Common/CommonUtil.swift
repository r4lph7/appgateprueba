//
//  CommonUtil.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class CommonUtil: NSObject {
    
    class func showAlert(_ alertTitle: String, alertContent: String, fromViewController: UIViewController, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertContent, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        fromViewController.present(alert, animated: true, completion: nil)
    }
}
