//
//  LoginViewController.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username : Field!
    @IBOutlet weak var password : Field!
    
    let loginViewModel = LoginViewModel.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBindings()
    }
    
    func configureBindings(){
        loginViewModel.onValidationSuccess = {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "logs", sender: self)
            }
        }
        loginViewModel.onValidationFailed = { message in
            DispatchQueue.main.async {
                CommonUtil.showAlert("Error", alertContent: message, fromViewController: self, actionTitle: "OK")
            }
        }
    }
    
    @IBAction func validateAccount(){
        guard let username = username.text,
              let password = password.text else {
            return
        }
        loginViewModel.validateAccount(username: username, password: password)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == username {
            password.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
