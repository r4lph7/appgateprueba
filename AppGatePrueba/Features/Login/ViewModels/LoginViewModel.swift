//
//  LoginViewModel.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import AppGateLibrary

class LoginViewModel {
    
    var onValidationSuccess: (() -> Void)?
    var onValidationFailed : ((_ message : String) -> Void)?
    
    static let shared = LoginViewModel()
    let presenter = AppGateLibraryPresenter.shared
    var user : User?
    
    func validateAccount(username: String, password: String){
        self.user = User(username: username, password: password)
        if let user = self.user {
            presenter.validateAccount(user) { [weak self] success in
                if success {
                    self?.onValidationSuccess?()
                }else{
                    self?.onValidationFailed?("Email/Password not valid")
                }
            }
        }
    }
}
