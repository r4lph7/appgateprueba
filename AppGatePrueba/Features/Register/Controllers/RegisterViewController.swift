//
//  RegisterViewController.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var username : Field!
    @IBOutlet weak var password : Field!
    
    let registerViewModel = RegisterViewModel.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        configureBindings()
    }
    
    func configureBindings(){
        registerViewModel.onRegisterSuccess = {
            self.registrationSucces()
        }
        registerViewModel.onRegisterFailed = { message in
            DispatchQueue.main.async {
                CommonUtil.showAlert("Error", alertContent: message, fromViewController: self, actionTitle: "OK")
            }
        }
    }
    
    func registrationSucces(){
        DispatchQueue.main.async {
            self.username.text = ""
            self.password.text = ""
            let alert = UIAlertController(title: "Success", message: "Account created", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func registerUser(){
        guard let username = username.text,
              let password = password.text else {
            return
        }
        registerViewModel.registerUser(username: username, password: password)
    }

}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == username {
            password.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
