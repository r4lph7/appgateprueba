//
//  RegisterViewModel.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import AppGateLibrary


class RegisterViewModel {
    static let shared = RegisterViewModel()
    
    var onRegisterSuccess: (() -> Void)?
    var onRegisterFailed : ((_ message : String) -> Void)?
    let presenter = AppGateLibraryPresenter.shared
    
    func registerUser(username: String, password: String){
        if validateEmail(username) {
            if validatePassword(password){
                presenter.registerUser(User(username: username, password: password)) { [weak self] success in
                    if success {
                        self?.onRegisterSuccess?()
                    }else{
                        self?.onRegisterFailed?("Account Already Exists")
                    }
                }
            }else{
                onRegisterFailed?("Invalid Password")
            }
        }else{
            onRegisterFailed?("Invalid Email")
        }
    }
    
    func validateEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func validatePassword(_ password: String) -> Bool{
        let passwordPred = NSPredicate(format: "SELF MATCHES %@ ", "^(?=.*[a-z])(?=.*[$@$#!%*?&])(?=.*[0-9])(?=.*[A-Z]).{8,}$")
        return passwordPred.evaluate(with: password)
    }
}
