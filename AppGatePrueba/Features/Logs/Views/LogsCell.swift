//
//  LogsCell.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import UIKit
import AppGateLibrary

class LogsCell: UITableViewCell {
    
    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var status : UILabel!
    
    var log : Log? {
        didSet{
            guard let log = log else { return }
            username.text = log.username
            date.text = log.timezone.time
            status.text = log.result.rawValue
            status.textColor = log.result == .denied ? UIColor.red : UIColor.green
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
