//
//  LogsViewController.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import UIKit
import AppGateLibrary

class LogsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let logsViewModel = LogsViewModel.shared
    var dataSource : [Log]! = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logsViewModel.getLogs()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configBindings()
        setupView()
    }
    
    func setupView(){
        let nib = UINib.init(nibName: "LogsCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "LogsCell")
    }
    
    func configBindings() {
        logsViewModel.onDidLoadLogs = {
            self.dataSource = self.logsViewModel.logs
        }
        logsViewModel.onDidFailLoad = { error in
            DispatchQueue.main.async {
                CommonUtil.showAlert("Error", alertContent: error, fromViewController: self, actionTitle: "OK")
            }
        }
    }

}

extension LogsViewController :  UITableViewDataSource, UITableViewDelegate{
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogsCell", for: indexPath) as! LogsCell
        cell.log = dataSource[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
}

