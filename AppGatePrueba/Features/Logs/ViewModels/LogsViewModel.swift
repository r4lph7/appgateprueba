//
//  LogsViewModel.swift
//  AppGatePrueba
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import AppGateLibrary


class LogsViewModel {
    static let shared = LogsViewModel()
    
    private(set) var logs: [Log] = []
    var onDidLoadLogs: (() -> Void)?
    var onDidFailLoad : ((_ error:String) -> Void)?
    
    func getLogs(){
        let presenter = AppGateLibraryPresenter.shared
        guard let logs = presenter.readLogs(),
              logs.count > 0 else {
            onDidFailLoad?("Error loading content")
            return
        }
        self.logs = logs
        onDidLoadLogs?()
    }
    
}
