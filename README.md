# AppgatePrueba

## Description

`AppgatePrueba` is a project built using Swift to give a solution to Appgate Challenge.
The project scope is save information in the safest space possible for later consultation.

## Dependencies

The project is composed of the following modules:

#### AppGatePrueba

This is the main application where the user can validate accounts, create them, and see a list of all the attempts.

#### AppGateLibrary

This framework is used to expose the API for the accounts creation and validation, It has as dependency the `AppGateStorage` library for storing sensitive data & logs.

#### AppGateStorage

This framework is used to expose an API for store the logs and the sensitive data, interacting with the **UserDefaults** and **Keychain**.

## How to Use

1. Clone or download the repository
2. Open `AppGatePrueba.xcworkspace` with Xcode.
3. Select AppgatePrueba on Target and Run.