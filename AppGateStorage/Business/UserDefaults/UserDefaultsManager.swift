//
//  UserDefaultsManager.swift
//  AppGateStorage
//
//  Created by RalphM on 8/08/21.
//

import Foundation

internal class UserDefaultsManager : NSObject {
    
    internal static func add(value: Data, account: String) throws {
        UserDefaults.standard.setValue(value, forKey: account)
    }

    internal static func retreive(account: String) throws -> Data? {
        return UserDefaults.standard.object(forKey: account) as? Data
    }
    
}
