//
//  Constants.swift
//  AppGateStorage
//
//  Created by RalphM on 7/08/21.
//

import Foundation

internal let service: String = "co.r4lph.AppGateStorage"

public enum Key : String{
    case accounts
    case logs
}

internal enum Errors: Error {
    case creatingError
    case operationError
}
