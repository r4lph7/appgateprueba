//
//  AppGateStoragePresenter.swift
//  AppGateStorage
//
//  Created by RalphM on 7/08/21.
//

import Foundation
public class AppGateStoragePresenter : NSObject {
    
    public static func set(value: Data, account: String, secure: Bool = true) throws {
        
        if secure {
            if try KeyChainManager.exists(account: account) {
                try KeyChainManager.update(value: value, account: account)
            } else {
                try KeyChainManager.add(value: value, account: account)
            }
        }else{
            try UserDefaultsManager.add(value: value, account: account)
        }
        
    }

    public static func get(account: String, secure: Bool = true) throws -> Data? {
        if secure {
            if try KeyChainManager.exists(account: account) {
                return try KeyChainManager.retreive(account: account)
            } else {
                throw Errors.operationError
            }
        }else{
            return try UserDefaultsManager.retreive(account: account)
        }
    }

    public static func delete(account: String) throws {
        if try KeyChainManager.exists(account: account) {
            return try KeyChainManager.delete(account: account)
        } else {
            throw Errors.operationError
        }
    }
    
    public static func deleteAll() throws {
        try KeyChainManager.deleteAll()
    }
}
